//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");

//Define your schema
const teamSchema = new Schema(
	{
		name: 
		{
			type: String,
			required: true,
			lowercase: true,
			trim: true
		}
	},
	{
		timestamps: true
	}
); 


teamSchema.virtual("members", {
	ref: "Member",
	localField: "_id", //team._id
	foreignField: "teamId"
})

//Export your model
module.exports = mongoose.model("Team", teamSchema);