//dependencies
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const validator = require('validator');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

//define your schema
const memberSchema = new Schema(
  {
    firstName: {
      type: String,
      trim: true,
      maxlength: 30,
      default: 'J'
    },
    lastName: {
      type: String,
      trim: true,
      maxlength: 30,
      default: 'Doe'
    },
    username: {
      type: String,
      required: true,
      trim: true,
      maxlength: 30,
      unique: true,
      validate(value) {
        if (!validator.isAlphanumeric(value)) {
          throw new Error(
            'Username must contain alphanumeric characters only.'
          );
        }
      }
    },
    position: {
      type: String,
      enum: ['instructor', 'hr', 'student', 'admin', 'ca'],
      required: false
    },
    age: {
      type: Number,
      min: [18, 'You must be at least 18 to join a team!'],
      required: false
    },
    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Email is invalid!');
        }
      }
    },
    password: {
      type: String,
      required: true,
      minlength: [5, 'Password must have at least 5 characters!']
    },
    teamId: {
      type: mongoose.Schema.Types.ObjectId,
      // required: true,
      ref: 'Team'
    },
    isActive: {
      type: Boolean,
      default: true
    }, 
    profilePic: {
      type: Buffer,
      default: undefined
    },
    tokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ]
  },
  {
    timestamps: true
  }
);

//HASH PASSWOD when creating and updating member
memberSchema.pre('save', async function(next) {
  const member = this;

  if (member.isModified('password')) {
    //salt
    const salt = await bcrypt.genSalt(10);
    //hash
    member.password = await bcrypt.hash(member.password, salt);
  }

  next();
});

//FIND BY CREDENTIALS USING STATICS
memberSchema.statics.findByCredentials = async (email, password) => {
  //check if email exists
  const member = await Member.findOne({ email });

  //CHALLENGE
  //Display error messages
  //Username or email
  if (!member) {
    throw new Error('Email is wrong!'); //for now
  }

  //comparing req pw vs unhashed db password
  const isMatch = await bcrypt.compare(password, member.password);

  if (!isMatch) {
    throw new Error('Wrong password!'); //for now
  }

  return member;
};

//GENERATE TOKEN
memberSchema.methods.generateAuthToken = async function() {
  const member = this;

  //1 - data to embed
  //2 - secret message
  //3 - options
  const token = jwt.sign({ _id: member._id.toString() }, 'batch40', {
    expiresIn: '2 days'
  });

  //save the token to our db
  member.tokens = member.tokens.concat({ token });
  await member.save();
  return token;
};

//HIDE PRIVATE DATA
memberSchema.methods.toJSON = function() {
  const member = this;

  //return RAW member object with ALL its fields
  const memberObj = member.toObject();

  //delete a private data
  delete memberObj.password;

  //EXERCISE 1: delete all tokens
  delete memberObj.tokens;

  //delete profilePic
  delete memberObj.profilePic;

  return memberObj;
};

//SET THE RELATION BETWEEN MEMBER AND TASK
memberSchema.virtual('tasks', {
  ref: 'Task',
  localField: '_id', //member._id
  foreignField: 'memberId'
});

memberSchema.plugin(uniqueValidator);
const Member = mongoose.model('Member', memberSchema);
//export your model
module.exports = Member;
