//Declare dependencies
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

//Define the schema
//description-String, teamId-String, isCompleted-Boolean
const taskSchema = new Schema(
	{
		description: {
			type: String,
			required: true,
			maxlength: 100
		},
		memberId: {
			type: mongoose.Schema.Types.ObjectId,
			required: true,
			ref: "Member"
		},
		isCompleted: {
			type: Boolean,
			default: false
		}
	},
	{
		timestamps: true
	}
);

//Export the model
module.exports = mongoose.model("Task", taskSchema);