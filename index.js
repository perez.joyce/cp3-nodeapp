//Declare dependencies
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const multer = require("multer");
const cors = require("cors");
const connectToRemoteDB = require("./config/db");

//Connect to local DB
// mongoose.connect("mongodb://localhost:27017/mern_tracker", {
// 	useNewUrlParser: true,
// 	useUnifiedTopology: true,
// 	useFindAndModify: false,
// 	useCreateIndex: true
// });
// mongoose.connection.once("open", ()=> {
// 	console.log("Now connected to local MongoDB");
// });

//Connect to remote DB
connectToRemoteDB()

//Apply Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

//Declare Models
const Team = require("./models/teams");
const Task = require("./models/tasks");
const Member = require("./models/members");

//Create Routes/Endpoints
//Transferred Routes
//Declare the resources
const teamsRoute = require("./routes/teams");
app.use("/teams", teamsRoute);
const membersRoute = require("./routes/members");
app.use("/members", membersRoute);
const tasksRoute = require("./routes/tasks");
app.use("/tasks", tasksRoute);


//Configure Multer
// const upload = multer({
// 	dest: "images/members",
// 	limits: {
// 		fileSize: 1000000 //max file size in bytes
// 	}, 
// 	fileFilter(req, file, cb) {
// 		//https://regex101.com
// 		if(!file.originalname.match(/\.(jpg|jpeg|JPEG|png|PNG)$/)){
// 			return cb(new Error("Please upload an image only"))
// 		}

// 		cb(undefined, true)
// 	}
// })

//Sample: Endpoint to upload a file
// app.post("/upload", upload.single("upload"), (req, res) =>{
// 	// try{
// 		res.send({ message: "Successfully uploaded image!"})
// 	// }catch(e){
// 	// 	//BAD REQUEST
// 	// 	res.status(400).send({ error: e.message })
// 	// }
// }, (error, req, res, next) => {
// 	res.status(400).send({ error: e.message })
// })


//Initialize the server
const port = process.env.PORT || 4000;
app.listen(port, () => {
	console.log(`Now listening to port ${port} :)`);
});