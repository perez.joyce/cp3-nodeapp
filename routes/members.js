//declare your dependencies
const Member = require('../models/members');
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const auth = require('../middleware/auth');
const multer = require("multer");
const sharp = require("sharp");


// test change

// UPLOAD IMAGE
const upload = multer({
  // dest: "images/members",
  limits: {
    fileSize: 1000000
  },
  fileFilter(req, file, cb) {
    if(!file.originalname.match(/\.(jpg|jpeg|png|PNG|JPEG)$/)){
      return cb(new Error("Please upload an image only."))
    }

    cb(undefined, true)
  }
})

// UPLOAD AN IMAGE
// localhost:port/members/upload
router.post("/upload", upload.single("upload"), auth, async (req, res) => {

  const buffer = await sharp(req.file.buffer)
    .resize({
      width: 50,
      height: 50
    })
    .png()
    .toBuffer();

  req.member.profilePic = buffer;
  await req.member.save();

  // res.send({ message: "Successfully uploaded image!"})
  res.send(req.member);
}, (error, req, res, next) => {
  res.status(400).send({ error: error.message })
})

// DELETE AN IMAGE
// ACTIVITY


// ===================================================

//define your routes/endpoints
//CREATE
router.post('/', async (req, res) => {
  console.log("Create a member")
  const member = new Member(req.body);
  try {
    await member.save();
    console.log(member)
    res.status(201).send(member);
  } catch (e) {
    console.log(e)
    res.status(500).send(e.message);
  }
});

//GET ALL 
router.get('/', async (req, res) => {
  console.log(req.query);
  try {
    console.log("get all members")
    const members = await Member.find(req.query).populate({
      path: 'teamId',
      // select: ['name', 'firstName']
      select: 'name'
    })
    
    res.status(200).send(members);
  } catch (e) {
    res.status(500).send(e.message);
  }
});

//GET LOGIN USER'S PROFILE
router.get("/me", auth, async (req, res) => {
  console.log("get my profile")
  res.send(req.member)
})

//LOGIN
router.post('/login', async (req, res) => {
  console.log("test")
  try {
    //submit email and password
    // const member = await Member.findByCredentials(
    //   req.body.email,
    //   req.body.password
    // );
    
    // const value = req.body.email;

    const member = await Member.findOne({ email: req.body.email });

    if(!member) {
    	return res.send({"message" : "Invalid login credentials!"}) //for now
    }

    // //comparing req pw vs unhashed db password
    const isMatch = await bcrypt.compare(req.body.password, member.password);

    if(!isMatch) {
    	return res.send({ "message" : "Invalid login credentials!"}) //for now
    }

    //Generate a token
    const token = await member.generateAuthToken();

    res.send({ member, token });
    // res.send(member);
    // console.log(member);
  } catch (e) {
    res.status(500).send(e);
  }
});

//LOGOUT ALL

//UPDATE (own profile)
router.patch('/me', auth, async (req, res) => {
  const updates = Object.keys(req.body);

  const allowedUpdates = ['firstName', 'lastName', 'position', 'password', 'teamId'];

  const isValidUpdate = updates.every(update =>
    allowedUpdates.includes(update)
  );

  if (!isValidUpdate) {
    return res.status(400).send({ error: 'Invalid update' });
  }

  try {
    //mano mano
    updates.map(update => (req.member[update] = req.body[update]));

    await req.member.save();
    res.send(req.member);
  } catch (e) {
    res.status(500).send(e);
  }
});

//UPDATE MEMBER'S POSITION AND TEAMID
router.patch("/:id", auth, async (req, res) => {

  const updates = Object.keys(req.body)
  const allowedUpdates = ["position", "teamId"]

  const isValidUpdate = updates.every(update => allowedUpdates.includes(update))

  if(!isValidUpdate) {
    return res.status(400).send({ error: "Invalid update!"})
  }

  try {
    const member = await Member.findByIdAndUpdate(req.params.id, req.body, { new: true})

    res.send(member)
  } catch(e) {
    res.status(500).send(e)
  }

})




// DISPLAY AN IMAGE 
//-> who can view profilePics? Anyone: not logged in and not owners
// localhost:port/members/:id/upload
router.get("/:id/upload", async (req, res) => {
  try {
    const member = await Member.findById(req.params.id);

    if(!member || !member.profilePic) {
      return res.status(404).send("Member or Profile Pic Doesn't exist")
    }

    //SEND BACK THE CORRECT DATA
    //TELL THE CLIENT WHAT TYPE OF DATA IT WILL RECEIVE
    res.set("Content-Type", "image/png")
    res.send(member.profilePic);
  } catch(e) {
    res.status(500).send(e)
  }
})



//DELETE ONE MEMBER
router.delete("/:id", auth, async(req, res) => {
  try{
    const member = await Member.findByIdAndUpdate(req.params.id, 
      { isActive: false}, 
      { new: true})

    res.send(member)
  } catch (e) {
    res.status(500).send(e)
  }
})


//GET ONE: /members/:id
router.get("/:id", auth, async (req, res) => {
  // res.send("get one")

  try{
    const member = await Member.findById(req.params.id).populate({
      path: "teamId",
      select: "name"
    })

    if(!member) {
      return res.status(404).send()
    }

    res.send(member)

  } catch(e){
    res.status(500).send(e)
  }
})


//export your routes/endpoints
module.exports = router;
