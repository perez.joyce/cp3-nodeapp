const Task = require("../models/tasks");
const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");

// CREATE A TASK
router.post("/", auth, async(req, res) => {
	const task = new Task({
		//use SPREAD OPERATOR to copy the body 
		...req.body,
		memberId: req.member._id
	})

	try {
		await task.save()
		//CREATED
		res.status(201).send(task)
	} catch (e) {
		//SERVER ERROR
		res.status(500).send(e)
	}
})

// GET ALL TASKS OF LOGGED IN MEMBER
router.get("/me", auth, async (req, res) => {
	// console.log("get all tasks of logged in member")
	// console.log(req.query)

	const match = {};
	const sort = {};

	if(req.query.isCompleted) {
		match.isCompleted = req.query.isCompleted
	}

	// console.log(req.query.sortBy)
	// console.log(req.query.sortBy.split(":"))

	if(req.query.sortBy) {
		const parts = req.query.sortBy.split(":");
		sort[parts[0]] = parts[1] === "desc" ? -1 : 1 
	}

	console.log(sort)

	try {
		await req.member
		.populate({
			path: "tasks",
			match, //match: match
			options: {
				limit: parseInt(req.query.limit),
				skip: parseInt(req.query.skip),
				sort //sort: sort
			}
		})
		.execPopulate();

		res.send(req.member.tasks)
	} catch (e) {
		res.status(500).send(e)
	}
})

// GET A TASK

// GET ALL TASKS
router.get("/", auth, async (req, res) => {
	try{
		const tasks = await Task.find()

		if(!tasks) {
			return res.status(404).send({ message: "No tasks found."})
		}

		return res.send(tasks)
	} catch(e) {
		res.status(500).send(e)
	}
})




module.exports = router;