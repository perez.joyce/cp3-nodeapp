//Declare dependecies and model
const Team = require("../models/teams");
const express = require("express");
const router = express.Router(); //to handle routing
const auth = require("../middleware/auth");


//1) CREATE
router.post("/", async (req, res) => {
	
	const team = new Team(req.body);

	//save to db
	try {
		await team.save();
		res.send(team);
	} catch(e) {
		res.status(400).send(e)
	}
})

//2) GET ALL TEAMS
router.get("/",  auth, async (req, res) => {
	console.log("get all teams")	
	try {
		const teams = await Team.find();
		return res.status(200).send(teams);
		console.log("with return");
	} catch (e) {
		console.log("error")
		return res.status(404).send(e);
	}
});

//3) GET ONE
router.get("/:id", auth, async (req, res) => {
	
	const _id = req.params.id;

	try {
		const team = await Team.findById(_id);
		
		if(!team) {
			// not working
			return res.status(404).send("404 ka tsong")
		}


		//populate team with its members

		return res.send(team) // return the members of the team
	} catch (e) {
		console.log("error 500")
		return res.status(500).send(e);
	}
});

//4) UPDATE ONE
router.patch("/:id", async (req, res) => {
	// return res.send("update a team");
	const _id = req.params.id;

	// Team.findByIdAndUpdate(_id, req.body, { new: true })
	// 	.then((team) => {
	// 		if(!team) {
	// 			//NOT FOUND
	// 			return res.status(404).send(e)
	// 		}
	// 		return res.send(team)
	// 	})
	// 	.catch((e) => {
	// 		return res.status(500).send(e)
	// 	})

	try {
		const team = await Team.findByIdAndUpdate(_id, req.body, { new: true })

		if(!team) {
			return res.status(404).send("test");
		}

		res.send(team);

	} catch (e) {
		return res.status(500).send(e);
	}

});

//5) DELETE ONE
router.delete("/:id", async (req, res) => {
	// return res.send("delete a team");

	const _id = req.params.id;

	try {
		const team = await Team.findByIdAndDelete(_id);
		if(!team) {
			return res.status(404).send("Team doesn't exist");
		}

		res.send(team);
	} catch(e) {
		res.status(500).send(e.message)
	}
});

module.exports = router;

//1. transfer routes to a diff resource file
//2. declare model
//3. declare dependency
//4. declare router
//5. replace "app" with "router"
//6. export router

//7. go back to app.js
//8. import resource file
//9. declare resource. employ DRY principle.

//10. go back to resource file and remove excess characters in endpoints
//11. use postman to test if your code still works